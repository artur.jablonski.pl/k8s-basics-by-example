import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.server.HttpApp;
import akka.http.javadsl.server.Route;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Value;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class App extends HttpApp
{
  public static void main(String[] args)
    throws ExecutionException, InterruptedException
  {
    new App().startServer("0.0.0.0", 8080);
  }

  private String getLocalIp()
  {
    try {
      return InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException e) {
      throw new RuntimeException(e);
    }
  }

  @Value
  @JsonPropertyOrder({ "requestedTo", "servedBy", "payload" })
  private static class Response
  {
    String requestedTo;
    String servedBy;
    String payload;
  }

  @Override
  protected Route routes()
  {
    return
      get(
        () ->
          pathEndOrSingleSlash(
            () -> extractHost(
              h -> completeOK(
                new Response(h, getLocalIp(), "Hello from backend!!!"),
                Jackson.marshaller()
              )
            )
          )
      );
  }
}
