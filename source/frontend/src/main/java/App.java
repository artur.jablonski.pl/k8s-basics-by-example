import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.server.HttpApp;
import akka.http.javadsl.server.Route;
import akka.stream.Materializer;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Value;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class App extends HttpApp
{
  private static final String BACKEND_HOST = "backend";
  private static final Integer BACKEND_PORT = 8080;

  public static void main(String[] args)
    throws ExecutionException, InterruptedException
  {
    new App().startServer("0.0.0.0", 8080);
  }

  private String getLocalIp()
  {
    try {
      return InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException e) {
      throw new RuntimeException(e);
    }
  }

  @Value
  @JsonPropertyOrder({ "requestedTo", "servedBy", "payload" })
  private static class BackendResponse
  {
    String requestedTo;
    String servedBy;
    String payload;
  }

  @Value
  @JsonPropertyOrder({ "requestedTo", "servedBy", "payload" })
  private static class Response
  {
    String requestedTo;
    String servedBy;
    BackendResponse payload;
  }

  @Override
  protected Route routes()
  {
    return
      get(
        () ->
          concat(
            pathEndOrSingleSlash(
              () -> extractHost(
                h ->
                  completeOKWithFuture(
                    Http.get(systemReference.get())
                        .singleRequest(HttpRequest.create(
                          String.format("http://%s:%d/",
                                        BACKEND_HOST,
                                        BACKEND_PORT)))
                        .thenApply(HttpResponse::entity)
                        .thenCompose(
                          e ->
                            Jackson.unmarshaller(BackendResponse.class)
                                   .unmarshal(e,
                                              Materializer
                                                .createMaterializer(
                                                  systemReference.get()))
                        )
                        .thenApply(
                          b -> new Response(h, getLocalIp(), b)
                        ),
                    Jackson.marshaller()
                  )
              )
            ),
            path("crashme",
                 () -> {
                   System.exit(1);
                   return complete("will never get here");
                 }
            )
          )
      );
  }
}
