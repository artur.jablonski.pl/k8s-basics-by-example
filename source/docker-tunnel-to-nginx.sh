#!/bin/bash
nginx_port=80
local_port=8081
node_port=$(kubectl get service -n ingress-nginx ingress-nginx -o=jsonpath="{.spec.ports[?(@.port == ${nginx_port})].nodePort}")
docker run -d --name kind-nginx-proxy-${local_port} \
  --publish 127.0.0.1:${local_port}:${local_port} \
  --link kind-control-plane:target \
  alpine/socat -dd \
  tcp-listen:${local_port},fork,reuseaddr tcp-connect:target:${node_port}
