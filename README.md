# Introduction to Kubernetes for developers by example

In this post I will go through the basics of deploying and running apps in
Kubernetes (k8s) - an open source container orchestration platform. 
I will be using a bit oversimplified, but practical 
example of a microservice type of application that exposes a HTTP endpoint 
to the outside world.

No prior k8s knowledge is required. Various aspects of the k8s architecture 
and concepts will be introduced as we progress with the application.

The example code is contenerized using Google jib plugin for gradle that 
allows for smart docker image builds of java apps without manually creating Dockerfile.

HTTP in the sample application (both
server and client side) is managed by Akka HTTP library.

All examples are run against kubernetes-in-docker (aka. KIND) multinode cluster
run locally. KIND is mainly for testing k8s itself. It has some limitations comparing
to a full blown k8s cluster, but it's good enough for the purpose of this post
and you don't need access to a real cluster elsewhere to try it out. 

This is what you need installed to run the examples:

1. Kind
2. Docker (requirement for Kind)
3. kubectl (k8s API CLI client to manage k8s clusters)

There's accompanied [git repository](https://gitlab.com/artur.jablonski.pl/k8s-basics-by-example) with all the sourcecode. Different stages
of the application development can be built by checking out appropriate git tag that 
will be indicated in the text like this: `[git checkout tags/<tag>]`. 

All shell commands are executed from `<git_repo_root>/source` directory. 

`kubectl` has been aliased to `k` for brevity, which is a common practice.

## Creating multinode KIND cluster

Let's create a multinode KIND cluster based on configuration provided by the project website:

`[git checkout tags/cluster]`
```bash
$> kind create cluster --config cluster.yaml
```

If all went well, you should see 3 KIND docker containers running:

```bash
$> docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                       NAMES
f5f6dc1a47cf        kindest/node:v1.16.3   "/usr/local/bin/entr…"   4 minutes ago       Up 3 minutes                                    kind-worker2
ea5c42808611        kindest/node:v1.16.3   "/usr/local/bin/entr…"   4 minutes ago       Up 3 minutes                                    kind-worker
0bfd0b7bd7fd        kindest/node:v1.16.3   "/usr/local/bin/entr…"   4 minutes ago       Up 3 minutes        127.0.0.1:43501->6443/tcp   kind-control-plane
```

Each container is a node of the k8s cluster with `kind-control-plane` being the master node
and `kind-worker` and `kind-worker2` being worker nodes.

Also, the `kubectl` should automatically be configured to talk to the created KIND cluster:

```bash
$> k cluster-info
Kubernetes master is running at https://127.0.0.1:33605
KubeDNS is running at https://127.0.0.1:33605/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

## Pods

Let's start with a single contenerized application we will call **frontend**, 
that will expose two HTTP endpoints:

1. HTTP GET to `/` will respond with the following json:
```json
{
 "requestedTo": "<host name the http request was sent to>",
 "servedBy" : "<IP where the request was handled>",
 "payload" : "Hello!!!"
} 
```
2. HTTP GET to `/crashme` will crash the application (very useful) 

Let's build the image:

`[git checkout tags/frontend-init]`
```bash
$> ./frontend/gradlew -p frontend clean jibDockerBuild 
```
                          
You should see the image in your local docker registry:

```bash
$> docker images
k8s-by-example/frontend 0.0.1 2b349671ca44 
```

In k8s cluster every node needs to be able to pull docker images that are referenced by pods.
In KIND you need to load images manually like this for the nodes to be able to resolve them:

```bash
$> kind load docker-image k8s-by-example/frontend:0.0.1
Image: "k8s-by-example/frontend:0.0.1" with ID "sha256:2b349671ca440ba8f8ee4fe1f266e71ca2ca921d1d0d03c2259b18f604c48c5a" not present on node "kind-control-plane"
Image: "k8s-by-example/frontend:0.0.1" with ID "sha256:2b349671ca440ba8f8ee4fe1f266e71ca2ca921d1d0d03c2259b18f604c48c5a" not present on node "kind-worker"
Image: "k8s-by-example/frontend:0.0.1" with ID "sha256:2b349671ca440ba8f8ee4fe1f266e71ca2ca921d1d0d03c2259b18f604c48c5a" not present on node "kind-worker2"
```

So how can we get that container up to k8s? Meet `Pod`, which is the smallest deployment unit in k8s. 
Like all other k8s objects a pod can be defined using a `yaml` file:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: frontend
  labels:
    app: frontend
spec:
  containers:
    - image: k8s-by-example/frontend:0.0.1
      name: frontend
      resources: {}
```

The important bits are: 
- name: a namespace wide unique name for the pod.
- labels: arbitrary key-value pairs associated with the pod.
- containers: specs for all the containers being part of this pod. Here we have just one
(which is the recommended way)

We can now deploy the pod:

`[git checkout tags/frontend-pod]`
```bash
$> k apply -f frontend/k8s/pod.yml
pod/frontend created
```

Let's see how it went:

```bash
$> k get pods                                                                               
NAME       READY   STATUS    RESTARTS   AGE
frontend   1/1     Running   0          2m1s
```

We can get more information about the pod like this:

```bash
$> k describe pod frontend
[cut]
Node:         kind-worker2/172.17.0.4
IP:           10.244.2.2
```

Here I filtered two interesting bits of information: `Node`, which is 
a cluster node where the pod is running and `IP` which is IP that the pod is assigned.

This is a good moment to mention the networking setup in k8s cluster. 
Basically all resources get their IP addressess from the same IP network. 
This means that one resource can reach another nomatter where the two are
in the cluster. The IP network by default is accessible only from within the cluster.
"Within the cluster" means either from container inside a pod or from a cluster node.

Let's try that with our KIND cluster. The pod is running on `kind-worker2` so let's
run `curl` from `kind-worker` node just to prove the point.

```bash
$> docker exec -it kind-worker curl http://10.244.2.2:8080/
{"requestedTo":"10.244.2.2","servedBy":"10.244.2.2","payload":"Hello!!!"}
```

Since we're calling the pod directly the `requestedTo` and `servedBy` are the same.

So far, so good. Let's see what happens if I crash a container inside the pod with 
our secret `/crashme` endpoint:

```bash
$> docker exec -it kind-worker curl http://10.244.2.2:8080/crashme
curl: (52) Empty reply from server
```

```bash
$> k get pods
NAME            READY   STATUS    RESTARTS   AGE
frontend        1/1     Running   1          16h
```

The `RESTARTS` counter has been incremented, and our container is back online.
This is thanks to the default restart policy our pod has installed.

That's great, but what if I accidently delete the pod? Or perhaps even worse,
what if the whole node the pod runs on goes down? In such case we're out of luck.
The pod will not be automatically restarted on some other healthy node in the cluster. 
For that we need a controller.

## `ReplicaSet`

Meet `ReplicaSet`: one of available controllers that we could use to make sure
there's always given number of our pods running.

This is how such `ReplicaSet` can look like:

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: frontend
  labels:
    app: frontend
spec:
  replicas: 3
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - image: k8s-by-example/frontend:0.0.1
          name: frontend
          resources: {}
```

We specify how many replicas of the pod we want and we give
a template for pod creation. The `selector` part tells `ReplicaSet` which pods
it should look after.

Let's give it a spin:

`[git checkout tags/frontend-replicaset]`
```bash
$> k apply -f frontend/k8s/replicaset.yaml
replicaset.apps/frontend created 
$> k get pods
NAME             READY   STATUS    RESTARTS   AGE
frontend         1/1     Running   1          15m
frontend-8vxzv   1/1     Running   0          2m53s
frontend-gkpv9   1/1     Running   0          2m53s
```

`ReplicaSet` is smart enough to know that there was already one pod with specified
label running so it started another 2 to reach the desired number of 3.

Let's now "accidently" delete a pod:

```bash
$> k delete pod frontend
pod "frontend" deleted
$> k get pods
NAME             READY   STATUS    RESTARTS   AGE
frontend-8vxzv   1/1     Running   0          6m53s
frontend-gkpv9   1/1     Running   0          6m53s
frontend-qlzzf   1/1     Running   0          30s
```

A new pod has been put in place by the `ReplicaSet`. Cool!

This is looking good, but since we know that microservices is all the rage nowadays
we decided to rearchitecture our app so that the frontend component will call another
component called **backend** to provide it with the payload to pass to the calling client.

The backend component when called on `/` with HTTP GET will respond:

```json
{
 "requestedTo": "<host name the http request was sent to>",
 "servedBy" : "<IP where the request was handled>",
 "payload" : "Hello from backend!!!"
} 
```

We will go through the same process of building an image, loading it into KIND and
creating a `ReplicaSet` for it:

`[git checkout tags/backend-init]`
```bash
$> ./backend/gradlew -p backend clean jibDockerBuild
$> kind load docker-image k8s-by-example/backend:0.0.1
$> k apply -f backend/k8s/replicaset.yaml
$> k get pods
NAME             READY   STATUS    RESTARTS   AGE
backend-4d92j    1/1     Running   0          6s
backend-6xl9l    1/1     Running   0          6s
backend-vkg2x    1/1     Running   0          6s
frontend-8vxzv   1/1     Running   0          19m
frontend-gkpv9   1/1     Running   0          19m
frontend-qlzzf   1/1     Running   0          13m
```

We have now 3 frontend and 3 backend pods running.
What we want now is for the frontend pods to call the backend pods, but...
how will they know how to find them? The IPs even though from the same
network are assigned dynamically and can change with time (if for example node crashes
and new pod is put in place). Meet `Service`.

## Service

`Service` is just the thing we need here. It can use label selector (just like `ReplicaSet`)
to expose a group of pods under single IP address and name and faciliate load balancing between 
them:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: backend
  labels:
    app: backend
spec:
  selector:
    app: backend
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
```

The name of the service is `backend` and it targets all pods with `app: backend` label. 
We map port 8080 of the service to port 8080 of pods.

Let's go ahead and create the service:

`[git checkout tags/backend-service]`
```bash
$> k apply -f  backend/k8s/service.yaml
service/backend created
$> k get services
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
backend      ClusterIP   10.96.131.21   <none>        8080/TCP   44s
```

The service created has its own IP address distinct from the pods it's targeting.
We can use that IP to hit the pods (again we need to be inside the cluster to reach
the IP):

```bash
$> docker exec -it kind-worker curl http:/10.96.131.21:8080/
{"requestedTo":"10.96.131.21","servedBy":"10.244.2.4","payload":"Hello from backend!!!"}

$> docker exec -it kind-worker curl http:/10.96.131.21:8080/
{"requestedTo":"10.96.131.21","servedBy":"10.244.2.5","payload":"Hello from backend!!!"}
```

We can see the load balancing effect here. both requests were sent to `10.96.131.21`
but were served by two different pods: `10.244.2.4` and `10.244.2.5`.

This is great, but there's something way cooler happening as well here. K8s can run an iternal DNS
server that registers a service under its name. KIND cluster has such DNS
server running by default:

```bash
$> k get pods --all-namespaces
NAMESPACE     NAME                                         READY   STATUS    RESTARTS   AGE
default       backend-4d92j                                1/1     Running   0          8h
default       backend-6xl9l                                1/1     Running   0          8h
default       backend-vkg2x                                1/1     Running   0          8h
default       frontend-8vxzv                               1/1     Running   0          8h
default       frontend-gkpv9                               1/1     Running   0          8h
default       frontend-qlzzf                               1/1     Running   0          8h
kube-system   coredns-5644d7b6d9-29xzg                     1/1     Running   0          9h
kube-system   coredns-5644d7b6d9-7lj66                     1/1     Running   0          9h
[cut]
```

**NOTE on namespaces**:

When we execute `k get pods` we only get a list od pods running in a namespace that `kubectl`
is currently configured with, which in our case is `default`. If we pass `--all-namespaces` we get pods
accross all the cluster. Namespaces are used for grouping related pods together and introduce
some isolation between different groups. Some of the K8s parts run as pods as well in their
own namespace. Here we can see two pods called `coredns-*` running in `kube-system` namespace.

The DNS is exposed in the cluster as a service:

```bash
$> k get services --all-namespaces
[cut]
NAMESPACE     NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)                  AGE
kube-system   kube-dns     ClusterIP   10.96.0.10     <none>        53/UDP,53/TCP,9153/TCP   9h
```

Every container running in any pod in the cluster is automatically configured by k8s to use
that DNS server to resolve host names.

You could trust my word for it, but why would you? Let's see that this is the case.
First let's get a shell in one of the worker nodes:

```bash
$> docker exec -it kind-worker2 /bin/bash
root@kind-worker2:/# 
```

Now let's list all the containers running here:

```bash
root@kind-worker2:/# crictl ps
CONTAINER           IMAGE               CREATED             STATE               NAME                ATTEMPT             POD ID
d030f1aee830c       ba0ab09f900b1       8 hours ago         Running             backend             0                   06761858cd243
72a4a6ceff3d7       ba0ab09f900b1       8 hours ago         Running             backend             0                   216c7e5ec5895
86b561f301f96       8d88832b9d794       9 hours ago         Running             frontend            0                   7a7eeb0c6d495
e44dfe9f82316       f4fd1d7052b4e       10 hours ago        Running             kube-proxy          0                   357e9455a0b39
c39a61c516243       aa67fec7d7ef7       10 hours ago        Running             kindnet-cni         0                   24b626c1a9276
```

Let's pick the first `backend` container and inspect it. The output is quite long, 
so I will cut it jus to show the important bit:

```bash
root@kind-worker2:/# crictl inspect d030f1aee830c
[cut]
{
          "destination": "/etc/resolv.conf",
          "type": "bind",
          "source": "/var/lib/containerd/io.containerd.grpc.v1.cri/sandboxes/06761858cd243b1cdff4369f7fc9497a6465558f5e0de36724b66b1dae6a2811/resolv.conf",
          "options": [
            "rbind",
            "rprivate",
            "rw"
          ]
        },
```

The section I showed here is a mount point inside the container for `/etc/resolv.conf`
which is where DNS servers are configured in GNU/Linux systems. If we cat the source file
we'll see the k8s DNS there:

```bash
root@kind-worker2:/# cat /var/lib/containerd/io.containerd.grpc.v1.cri/sandboxes/06761858cd243b1cdff4369f7fc9497a6465558f5e0de36724b66b1dae6a2811/resolv.conf
search default.svc.cluster.local svc.cluster.local cluster.local
nameserver 10.96.0.10
options ndots:5
```

Alright! So with all these bits in place, our frontend should be able to call the backend
by using the service name. We updated our frontend to do just that and loaded the new version
into the KIND cluster:

`[git checkout tags/frontend-calling-backend]`
```bash
$> ./frontend/gradlew -p frontend clean jibDockerBuild
$> ~/kind load docker-image k8s-by-example/frontend:0.0.2 
```

So how do we roll it out? Updating the existing `ReplicaSet` with new version 
of the image will not work. We need to delete the exising `ReplicaSet` and 
create a new one with the new image version. OR we can let k8s to do that for us.
Meet `Deployment`.

## `Deployment`

`Deployment`'s job is to create and manage `ReplicaSet` objects. It allows for smart
rollouts with zero downtime, rollbacks to previous version, stopping deployment if new
pods don't start, etc. 

Let's first create a `Deployment` for the `k8s-by-eample/frontend:0.0.1` image:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
  labels:
    app: frontend
spec:
  replicas: 3
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
        - image: k8s-by-example/frontend:0.0.1
          name: frontend
          resources: {}
```

and apply it:

`[git checkout tags/frontend-deployment-0.0.1]`
```bash
$> k apply -f frontend/k8s/deployment.yaml
deployment.apps/frontend created
$> k get pods
NAME             READY   STATUS    RESTARTS   AGE
backend-4d92j    1/1     Running   0          19h
backend-6xl9l    1/1     Running   0          19h
backend-vkg2x    1/1     Running   0          19h
frontend-8vxzv   1/1     Running   0          19h
frontend-gkpv9   1/1     Running   0          19h
frontend-qlzzf   1/1     Running   0          19h
$> k get replicaset
NAME       DESIRED   CURRENT   READY   AGE
backend    3         3         3       19h
frontend   3         3         3       19h
```

`Deployment` is smart enough to know that there is already a `ReplicaSet` that 
matches the pods,

Let's now update the version of the image in `Deployment` to 0.0.2 and apply it again:

`[git checkout tags/frontend-deployment-0.0.2]`

```bash
$> k apply -f frontend/k8s/deployment.yaml
deployment.apps/frontend configured
$> k get replicaset
NAME                 DESIRED   CURRENT   READY   AGE
backend              3         3         3       19h
frontend             2         2         2       19h
frontend-5c7cf7cb5   2         2         1       2s
$> k get replicaset
NAME                 DESIRED   CURRENT   READY   AGE
backend              3         3         3       19h
frontend             0         0         0       19h
frontend-5c7cf7cb5   3         3         3       10s
```

Here we can see `Deployment` in action. It simulatniously scales down
pods from old `ReplicaSet` and scales up those from the new one until
there's no more old pods and the count of new ones is at the desired level.

So let's see how is our frontend service responding now:

```bash
$> docker exec -it kind-worker2 curl http:/10.244.2.12:8080/
{"requestedTo":"10.244.2.12",
 "servedBy":"10.244.2.12",
 "payload":{"requestedTo":"backend",
            "servedBy":"10.244.2.4",
            "payload":"Hello from backend!!!"}
}
```

Let's decipher the response. We called a frontend pod directly by its IP (10.244.2.12), so that's
why we have the same values in _requestedTo_ and _servedBy_ fields. The frontend called backend
by name "backend" name and it was served by backend pod with IP address 10.244.2.4.

Things are looking good, though so far we were hitting the frontend pods directly.
Let's fix it by adding a `Service` to the frontend pods:

`[git checkout tags/frontend-service]`
```bash
k apply -f frontend/k8s/service.yaml
service/frontend created

$> k get services
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
backend      ClusterIP   10.96.131.21   <none>        8080/TCP   12h
frontend     ClusterIP   10.99.35.58    <none>        8080/TCP   15s
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP    22h

$> docker exec -it kind-worker2 curl http:/10.99.35.58:8080/
{"requestedTo":"10.99.35.58",
 "servedBy":"10.244.2.12",
 "payload":{"requestedTo":"backend",
            "servedBy":"10.244.2.4",
            "payload":"Hello from backend!!!"}
}
```

If you hit the frontend service couple of times you will observe different frontend
and backend pods involved in response. 

Ok, so we we can hit the service from within the cluster by its IP, how about 
exposing it to the outside-of-cluster world?

## Ingress

A quick way to expose a service to the host with `kubectl` configured to talk to 
the cluster is to use `port-forward` command of `kubectl`: 

```bash
$> k port-forward services/frontend 8080:8080
```

Until we hit Ctrl+C, `kubectl` will maintain a tunnel opened from a local port 8080
to the frontend service. We can now hit the service directly (from a different shell):

```bash
$> curl http://localhost:8080 
{"requestedTo":"localhost",
 "servedBy":"10.244.2.12",
 "payload":{"requestedTo":"backend",
            "servedBy":"10.244.2.5",
            "payload":"Hello from backend!!!"}
}
```

This is a great way to quickly get to pods or services as a developer
where it's impractical (or impossible)
to get shell on one of the cluster nodes. In our setup it's perhaps not that useful since we 
can execute commands from the cluster nodes with `docker exec` command.

Let's now look into methods for exposing an endpoint to wider audience.

Let's look again at available services we have:

```bash
$> k get services
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
backend      ClusterIP   10.96.131.21   <none>        8080/TCP   12h
frontend     ClusterIP   10.99.35.58    <none>        8080/TCP   15s
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP    22h
```

The `TYPE` of our front and back services is `ClusterIP` which means that the 
service in question is only available from within the cluster. There
are two other types of services that we'll look at now: _NodePort_ and _Loadbalancer_

Let's go ahead and change our frontend service type to NodePort

`[git checkout tags/frontend-service-nodeport]`
```bash
$> k apply -f frontend/k8s/service.yaml
service/frontend configured
$> k get services
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
backend      ClusterIP   10.96.131.21   <none>        8080/TCP         25h
frontend     NodePort    10.99.35.58    <none>        8080:32431/TCP   12h
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          34h
```

The TYPE of the frontend service is now NodePort. Just like `kubectl port-forward`
tunnelled a local port to the service, NodePort tunnels a random port (here it is 32431) 
of ALL the cluster nodes to the service.

We can now hit the service like this:

```bash
$> docker exec -it kind-control-plane curl http:/localhost:32431/                                                         nie, 2 lut 2020, 10:44:26
{"requestedTo":"localhost",
 "servedBy":"10.244.2.12",
 "payload":{"requestedTo":"backend",
            "servedBy":"10.244.2.5",
            "payload":"Hello from backend!!!"}
}
```

We can also do the same from the other two cluster nodes:

```bash
$> docker exec -it kind-worker curl http:/localhost:32431/
$> docker exec -it kind-worker2 curl http:/localhost:32431/
```

This means that anything that can talk to the cluster nodes via network can talk to 
the service and that allows to setup a custom load balancing solution. In our
case it's not that useful, since our cluster nodes are docker containers that
don't expose any ports other then k8s API port.

`LoadBalancer` service type will use cloud provider
specific way of provisioning a load balancer and hook it up to the service. 
This is not that useful here since we are not running the cluster in any cloud infrastructure.

Finally, there's more generic and extensible way of exposing potentially multiple HTTP endpoints. 
Meet `Ingress`.

`Ingress` decouples the **WHAT** is exposed from **HOW** it is exposed.

The **HOW** part is looked after by Ingress Controller. There are many controllers available to use.
One of them is an Nginx Ingress Controller. KIND is not equipped with any controller by default.
I guess this is because KIND is meant for testing and not running production workloads, still,
some smart guys figured out how to install the nginx Ingress Controller on KIND and we'll quickly
go through a simplified process here just because... why not!

`[git checkout tags/nginx-ingress-controller]`
```bash
$> k apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.28.0/deploy/static/mandatory.yaml
$> k apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml
$> k apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/baremetal/service-nodeport.yaml
$> ./docker-tunnel-to-nginx.sh
```

For the full explanaition of the commands please read the blog post [here](https://banzaicloud.com/blog/kind-ingress/).

If all went well, we should have a localhost port 8081 mapped to nginx running in the cluster:

```bash
$> curl http://localhost:8081
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.17.7</center>
</body>
</html>
```

Now going back to **WHAT** part we need to create an Ingress resource:

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/use-regex: "true"
spec:
  rules:
  - http:
      paths:
      - path: /api/?$
        backend:
          serviceName: frontend
          servicePort: 8080
```

In the Ingress resource we can map multiple services to different paths, 
do rewrites, set paths for given hosts. Anything that you can do in standalone
nginx. Here we map `/api` path to our frontend service

Let's check if it works, we will also revert the type of our frontend service to
`ClusterIP`:

`[git checkout tags/nginx-ingress]`
```bash
$> k delete service frontend
service "frontend" deleted
$> k apply -f frontend/k8s/service.yaml
service/frontend created
$> k apply -f ingress.yaml
ingress.networking.k8s.io/test-ingress configured

$> curl http:/localhost:8081/api
{"requestedTo":"localhost","servedBy":"10.244.2.25","payload":{"requestedTo":"backend","servedBy":"10.244.2.19","payload":"Hello from backend!!!"}}
```

We could also, in the same Ingress resource expose our backend service:

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/use-regex: "true"
spec:
  rules:
  - http:
      paths:
      - path: /api/?$
        backend:
          serviceName: frontend
          servicePort: 8080
      - path: /backend/?$
        backend:
          serviceName: backend
          servicePort: 8080
```

`[git checkout tags/nginx-expose-backend]`
```bash
$> k apply -f ingress.yaml
ingress.networking.k8s.io/test-ingress configured

$> curl http://localhost:8081/backend
{"requestedTo":"localhost","servedBy":"10.244.1.5","payload":"Hello from backend!!!"}
```

## Summary

In this post we went through the basics of k8s from perspective of a developer. We developed 
and deployed a simple HTTP "frontend" endpoint that calls a "backend" endpoint to fulfill client 
request. As we progressed we learnt about k8s objects like Pod, ReplicaSet, Deployment, Service,
Ingress and how to create, manipulate and query them using `kubectl` CLI client for k8s API.
We covered networking setup in k8s and service discovery using internal DNS server. We touched
on namespaces and what they are used for. We also went through how to expose the service to 
the outside of cluster world. 

There's a lot of field left to cover and I hope to cover some of it in future posts.


## References:
- K8s documentation - https://kubernetes.io/docs/home/
- KIND cluster - https://kind.sigs.k8s.io/docs/user/quick-start/
- Google jib - https://github.com/GoogleContainerTools/jib
- Kubernetes Distilled talk at Devoxx - https://www.youtube.com/watch?v=l7lt6yYLvRo
- blog post on how to install nginx ingress controller in KIND - https://banzaicloud.com/blog/kind-ingress/
- Nginx ingress controller - https://kubernetes.github.io/ingress-nginx/ 
